# ==============================================================================
# Modified by Prashanth Gurunath Shivakumar
# email: pgurunat@usc.edu
# University of Southern California
# ==============================================================================
#
# Example Usages:
#
# 1. Baseline Word2vec
# python conf2vec.py --train_data train.sausage --eval_data questions-words.txt --words_file words.txt --embedding_size 256 --epochs_to_train 15 --learning_rate 0.1 --num_neg_samples 64 --batch_size 256 --window_size 4 --min_count 5 --subsample 1e-3 --save_path word2vec_folder --concurrent_steps 1 --mode 'word2vec' --conf_eval_data homophone_analogy.txt --score 1.0 --disable_lr_scheduler
#
# 2. Conf2vec
# python conf2vec.py --train_data train.sausage --eval_data questions-words.txt --words_file words.txt --embedding_size 256 --epochs_to_train 15 --learning_rate 0.1 --num_neg_samples 64 --batch_size 256 --window_size 4 --min_count 5 --subsample 1e-3 --save_path confusion2vec_folder --concurrent_steps 1 --mode 'inter-confusion' --conf_eval_data homophone_analogy.txt --score 1.0 --disable_lr_scheduler
#
# 3. Conf2vec with pretraining
# python conf2vec.py --train_data train.sausage --eval_data questions-words.txt --words_file words.txt --embedding_size 256 --epochs_to_train 15 --learning_rate 0.1 --num_neg_samples 64 --batch_size 256 --window_size 4 --min_count 5 --subsample 1e-3 --save_path confusion2vec_folder --concurrent_steps 1 --mode 'inter-confusion' --conf_eval_data homophone_analogy.txt --score 1.0 --disable_lr_scheduler --pretrain GoogleW2V.bin
#  
# 4. Fixed Word2vec Joint Optimization:
# python conf2vec.py --train_data train.sausage --eval_data questions-words.txt --words_file words.txt --embedding_size 256 --epochs_to_train 15 --learning_rate 0.1 --num_neg_samples 64 --batch_size 256 --window_size 4 --min_count 5 --subsample 1e-3 --save_path confusion2vec_folder --concurrent_steps 1 --mode 'hybrid' --conf_eval_data homophone_analogy.txt --score 1.0 --disable_lr_scheduler --fixed_embed_pretrain word2vec_pretrained_model --pretrain conf2vec_intra_model --branched_mode --hybrid_modes "inter-confusion,intra-confusion"
#
# 5. Unrestricted Joint Optimization:
# python conf2vec.py --train_data train.sausage --eval_data questions-words.txt --words_file words.txt --embedding_size 256 --epochs_to_train 15 --learning_rate 0.1 --num_neg_samples 64 --batch_size 256 --window_size 4 --min_count 5 --subsample 1e-3 --save_path confusion2vec_folder --concurrent_steps 1 --mode 'hybrid' --conf_eval_data homophone_analogy.txt --score 1.0 --disable_lr_scheduler --fixed_embed_pretrain word2vec_pretrained_model --pretrain conf2vec_intra_model --joint_mode --hybrid_modes "inter-confusion,intra-confusion"


"""Multi-threaded conf2vec mini-batched skip-gram model.

Trains the model described in:
(Prashanth Gurunath Shivakumar, et. al.) Confusion2Vec: Towards Enriching Vector Space Word Representations with Representational Ambiguities
https://arxiv.org/abs/1811.03199
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import sys
import threading
import time
import collections, re, random
import pickle

from six.moves import xrange  # pylint: disable=redefined-builtin

import numpy as np
import tensorflow as tf


flags = tf.app.flags

flags.DEFINE_string("save_path", None, "Directory to write the model and "
                    "training summaries.")
flags.DEFINE_string("train_data", None, "Training text file. "
                    "E.g., unzipped file http://mattmahoney.net/dc/text8.zip.")
flags.DEFINE_string(
    "eval_data", None, "File consisting of analogies of four tokens."
    "embedding 2 - embedding 1 + embedding 3 should be close "
    "to embedding 4."
    "See README.md for how to get 'questions-words.txt'.")
flags.DEFINE_string(
    "conf_eval_data", None, "File consisting of confusion analogies of four tokens."
    "evaluated in the same way as typical analogies.")
flags.DEFINE_string("words_file", None, "words.txt file.")
flags.DEFINE_string("restore", None, "Full path to checkpoint file to be restored. Default: Latest checkpoint")
flags.DEFINE_string("fixed_embed_pretrain", None, 'Path to google word2vec model (fixed) for initializing')
flags.DEFINE_string("pretrain", None, 'Path to embedding model (potentially trainable) for initializing')
flags.DEFINE_integer("initial_epoch", 1, "Starting epoch (Useful while resuming training")
flags.DEFINE_integer("embedding_size", 200, "The embedding dimension size for trainable embedding.")
flags.DEFINE_integer(
    "epochs_to_train", 15,
    "Number of epochs to train. Each epoch processes the training data once "
    "completely.")
flags.DEFINE_float("learning_rate", 0.2, "Initial learning rate.")
flags.DEFINE_integer("num_neg_samples", 100,
                     "Negative samples per training example.")
flags.DEFINE_integer("batch_size", 16,
                     "Number of training examples processed per step "
                     "(size of a minibatch).")
flags.DEFINE_integer("concurrent_steps", 12,
                     "The number of concurrent training steps.")
flags.DEFINE_integer("window_size", 5,
                     "The number of words to predict to the left and right "
                     "of the target word.")
flags.DEFINE_integer("min_count", 5,
                     "The minimum number of word occurrences for it to be "
                     "included in the vocabulary.")
flags.DEFINE_float("subsample", 1e-3,
                   "Subsample threshold for word occurrence. Words that appear "
                   "with higher frequency will be randomly down-sampled. Set "
                   "to 0 to disable.")
flags.DEFINE_string("mode", "word2vec", "Training mode ['confusion', 'inter-confusion', "
                   "'word2vec', 'intra-confusion']")
flags.DEFINE_string("score", "1.0", "Scores for confusion arcs: ['ASR', 'equal', '1.0'].")
flags.DEFINE_string("load_data", None, "Path to saved numpy dump for faster data loading.")
flags.DEFINE_string("hybrid_modes", "inter-confusion,intra-confusion", "Comman separated modes to be used for hybrid-training.")
flags.DEFINE_string("pretrain_embedding_name", "emb:0", "Name of the embedding name to be loaded for pretraining.")
flags.DEFINE_boolean(
    "interactive", False,
    "If true, enters an IPython interactive session to play with the trained "
    "model. E.g., try model.analogy(b'france', b'paris', b'russia') and "
    "model.nearby([b'proton', b'elephant', b'maxwell'])")
flags.DEFINE_boolean(
    "disable_lr_scheduler", False, "If true, uses constant learning rate.")
flags.DEFINE_boolean('pretrain_text', False, 'Set format of pretrain model to text opposed to binary')
flags.DEFINE_boolean('fixed_embed_pretrain_text', False, 'Set format of pretrain fixed embedding model to text opposed to binary')
flags.DEFINE_boolean('embed_trainable', True, 'Fix the pretrain embedding.')
flags.DEFINE_boolean('branched_mode', False, 'Concatenate embeddings from 2 models with one of the embedding being fixed and '
                    'the other having an option to learn')
flags.DEFINE_boolean('joint_mode', False, 'Concatenate embeddings from 2 models and jointly train')
flags.DEFINE_boolean('initialize_softmax', False, 'Use pretraining weights from softmax layer for initialization.')
flags.DEFINE_integer("statistics_interval", 5,
                     "Print statistics every n seconds.")
flags.DEFINE_integer("summary_interval", 5,
                     "Save training summary to file every n seconds (rounded "
                     "up to statistics interval).")
flags.DEFINE_integer("checkpoint_interval", 1,
                     "Checkpoint the model (i.e. save the parameters) every n "
                     "epochs.")

FLAGS = flags.FLAGS

if FLAGS.mode == "hybrid":
  num_modes = len(FLAGS.hybrid_modes.strip().split(","))
  if FLAGS.batch_size % num_modes != 0 or FLAGS.batch_size/num_modes % 2*FLAGS.window_size != 0:
    print('batch_size, window_size and number of hybrid_modes should have a multiple common divisor')
    sys.exit(1)

if FLAGS.joint_mode and FLAGS.branched_mode:
  print('ERROR: Both --joint-mode and --branched-mode cannot be passed at the same time.')
  sys.exit(1)

if FLAGS.joint_mode or FLAGS.branched_mode:
  if FLAGS.pretrain is None and FLAGS.fixed_embed_pretrain is None:
    print('--joint_mode and --branched_mode requires both --pretrain and --fixed_embed_pretrain to be specified')
    sys.exit(1)

class Options(object):
  """Options used by our word2vec model."""

  def __init__(self):
    # Model options.

    # Embedding dimension.
    self.emb_dim = FLAGS.embedding_size

    # Training options.
    # The training text file.
    self.train_data = FLAGS.train_data

    # Number of negative samples per example.
    self.num_samples = FLAGS.num_neg_samples

    # The initial learning rate.
    self._learning_rate = FLAGS.learning_rate
    # Number of epochs to train. After these many epochs, the learning
    # rate decays linearly to zero and the training stops.
    self.epochs_to_train = FLAGS.epochs_to_train

    # Concurrent training steps.
    self.concurrent_steps = FLAGS.concurrent_steps

    # Number of examples for one training step.
    self.batch_size = FLAGS.batch_size

    # The number of words to predict to the left and right of the target word.
    self.window_size = FLAGS.window_size

    # The minimum number of word occurrences for it to be included in the
    # vocabulary.
    self.min_count = FLAGS.min_count

    # Subsampling threshold for word occurrence.
    self.subsample = FLAGS.subsample

    # How often to print statistics.
    self.statistics_interval = FLAGS.statistics_interval

    # How often to write to the summary file (rounds up to the nearest
    # statistics_interval).
    self.summary_interval = FLAGS.summary_interval

    # How often to write checkpoints (rounds up to the nearest statistics
    # interval).
    self.checkpoint_interval = FLAGS.checkpoint_interval

    # Where to write out summaries.
    self.save_path = FLAGS.save_path
    if not os.path.exists(self.save_path):
      os.makedirs(self.save_path)

    # Eval options.
    # The text file for eval.
    self.eval_data = FLAGS.eval_data
    self.conf_eval_data = FLAGS.conf_eval_data

def flatten(x):
    if isinstance(x, collections.Iterable):
        return [a for i in x for a in flatten(i)]
    else:
        return [x]

def replace_apostrophe_index(model, vocabulary_size, reverse_dictionary):
  indices = []
  not_indices = []
  for idx in xrange(vocabulary_size):
    word = reverse_dictionary[idx].lower()
    if word not in model.vocab and word[-2:] == "'s" and word[:-2] in model.vocab:
      indices.append((idx, model.vocab[word[:-2]].index))
    elif word in model.vocab:
      indices.append((idx, model.vocab[word].index))
    else:
      not_indices.append(idx)
  return indices, not_indices

class Word2Vec(object):
  """Word2Vec model (Skipgram)."""

  def __init__(self, options, session):
    self._options = options
    self._session = session
    self._word2id = {}
    self._id2word = []
    self.prepare_data()
    self.build_graph()
    self.build_eval_graph()
    self.save_vocab()

  def read_analogies(self, eval_data):
    """Reads through the analogy question file.

    Returns:
      questions: a [n, 4] numpy array containing the analogy question's
                 word ids.
      questions_skipped: questions skipped due to unknown words.
    """
    questions = []
    questions_skipped = 0
    with open(eval_data, "rb") as analogy_f:
      for line in analogy_f:
        if line.startswith(b":"):  # Skip comments.
          continue
        words = line.strip().lower().split(b" ")
        ids = [self._word2id.get(w.strip()) for w in words]
        if None in ids or len(ids) != 4:
          questions_skipped += 1
        else:
          questions.append(np.array(ids))
    print("Eval analogy file: ", eval_data)
    print("Questions: ", len(questions))
    print("Skipped: ", questions_skipped)
    #self._analogy_questions = np.array(questions, dtype=np.int32)
    return np.array(questions, dtype=np.int32)

  def forward(self, examples, example_scores, labels):
    """Build the graph for the forward pass."""
    opts = self._options

    # Declare all variables we need.
    # Embedding: [vocab_size, emb_dim]

    if FLAGS.joint_mode:
      init_width = 0.5 / (opts.emb_dim + opts.fixed_emb_dim)
      emb = tf.Variable(
          tf.random_uniform(
              [opts.vocab_size, opts.emb_dim + opts.fixed_emb_dim], -init_width, init_width),
          name="emb", trainable=FLAGS.embed_trainable)
    else:
      init_width = 0.5 / opts.emb_dim
      emb = tf.Variable(
          tf.random_uniform(
              [opts.vocab_size, opts.emb_dim], -init_width, init_width),
          name="emb", trainable=FLAGS.embed_trainable)
    

    if FLAGS.branched_mode:
      init_width = 0.5 / opts.fixed_emb_dim
      fixed_emb = tf.Variable(
          tf.random_uniform(
              [opts.vocab_size, opts.fixed_emb_dim], -init_width, init_width),
          name="fixed_emb", trainable=False)
      self._emb = tf.concat([fixed_emb, emb], 1)
      sm_w_t = tf.Variable(
          tf.zeros([opts.vocab_size, opts.emb_dim + opts.fixed_emb_dim]),
          name="sm_w_t")
    elif FLAGS.joint_mode:
      self._emb = emb
      # Softmax weight: [vocab_size, emb_dim]. Transposed.
      sm_w_t = tf.Variable(
          tf.zeros([opts.vocab_size, opts.emb_dim+opts.fixed_emb_dim]),
          name="sm_w_t")
    else:
      self._emb = emb
      # Softmax weight: [vocab_size, emb_dim]. Transposed.
      sm_w_t = tf.Variable(
          tf.zeros([opts.vocab_size, opts.emb_dim]),
          name="sm_w_t")

    # Softmax bias: [vocab_size].
    sm_b = tf.Variable(tf.zeros([opts.vocab_size]), name="sm_b")

    # Weight initialization
    self.pretrain = tf.assign(emb, self.pretrain_placeholder, validate_shape=False)
    self.initialize_sm_w_t = tf.assign(sm_w_t, self.pretrain_placeholder, validate_shape=False)
    self.initialize_sm_b = tf.assign(sm_b, tf.squeeze(self.pretrain_placeholder), validate_shape=False)

    # Global step: scalar, i.e., shape [].
    self.global_step = tf.Variable(0, name="global_step")

    # Nodes to compute the nce loss w/ candidate sampling.
    #labels_matrix = tf.reshape(
    labels_matrix = tf.cast(labels,
                dtype=tf.int64)#,

    # [opts.batch_size * num_true])
    labels_flat = tf.reshape(labels, [-1])

    # Negative sampling.
    sampled_ids, _, _ = (tf.nn.fixed_unigram_candidate_sampler(
        true_classes=labels_matrix,
        num_true=self.max_confusion_length,
        num_sampled=opts.num_samples,
        unique=True,
        range_max=opts.vocab_size,
        distortion=0.75,
        unigrams=opts.vocab_counts)) #.tolist()))

    # Confusion Embeddings
    #embed = tf.nn.embedding_lookup(emb, examples)
    # Embeddings for examples: [batch_size, emb_dim]
    example_emb = tf.nn.embedding_lookup(emb, examples)
    example_emb = tf.transpose(example_emb, perm=[0,2,1])
    example_emb = tf.squeeze(tf.matmul(example_emb, example_scores))

    if FLAGS.branched_mode:
      self.pretrain_fixed = tf.assign(fixed_emb, self.pretrain_placeholder, validate_shape=False)
      example_fixed_emb = tf.nn.embedding_lookup(fixed_emb, examples)
      example_fixed_emb = tf.transpose(example_fixed_emb, perm=[0,2,1])
      example_fixed_emb = tf.squeeze(tf.matmul(example_fixed_emb, example_scores))
      example_emb = tf.concat([example_fixed_emb, example_emb], 1)

    # Weights for labels: [batch_size * num_true, emb_dim]
    true_w = tf.nn.embedding_lookup(sm_w_t, labels_flat)
    # Biases for labels: [batch_size * num_true, 1]
    true_b = tf.nn.embedding_lookup(sm_b, labels_flat)

    if FLAGS.mode == 'confusion':
      dim = tf.shape(true_w)[1:2]
      new_true_w_shape = tf.concat([[-1, self.max_confusion_length], dim], 0)
      # True logits: [batch_size, num_true, emb_dim]
      true_logits = tf.multiply(tf.expand_dims(example_emb, 1), tf.reshape(true_w, new_true_w_shape))
      # True logits: [batch_size, num_true]
      true_logits = tf.reshape(true_logits, tf.concat([[-1], dim], 0))
      true_logits = tf.reshape(tf.reduce_sum(true_logits, 1), [-1, self.max_confusion_length]) + tf.reshape(true_b, [-1, self.max_confusion_length])
    else:
      # True logits: [batch_size, 1]
      true_logits = tf.reduce_sum(tf.multiply(example_emb, true_w), 1) + true_b

    # Weights for sampled ids: [num_sampled, emb_dim]
    sampled_w = tf.nn.embedding_lookup(sm_w_t, sampled_ids)
    # Biases for sampled ids: [num_sampled, 1]
    sampled_b = tf.nn.embedding_lookup(sm_b, sampled_ids)


    # Sampled logits: [batch_size, num_sampled]
    # We replicate sampled noise labels for all examples in the batch
    # using the matmul.
    sampled_b_vec = tf.reshape(sampled_b, [opts.num_samples])
    sampled_logits = tf.matmul(example_emb,
                               sampled_w,
                               transpose_b=True) + sampled_b_vec
    return true_logits, sampled_logits

  def nce_loss(self, true_logits, sampled_logits):
    """Build the graph for the NCE loss."""

    # cross-entropy(logits, labels)
    opts = self._options
    true_xent = tf.nn.sigmoid_cross_entropy_with_logits(
        labels=tf.ones_like(true_logits), logits=true_logits)
    sampled_xent = tf.nn.sigmoid_cross_entropy_with_logits(
        labels=tf.zeros_like(sampled_logits), logits=sampled_logits)

    # NCE-loss is the sum of the true and noise (sampled words)
    # contributions, averaged over the batch.
    nce_loss_tensor = (tf.reduce_sum(true_xent) +
                       tf.reduce_sum(sampled_xent)) / opts.batch_size
    return nce_loss_tensor

  def optimize(self, loss):
    """Build the graph to optimize the loss function."""

    # Optimizer nodes.
    # Linear learning rate decay.
    opts = self._options
    self.lr_op = tf.assign(self._learning_rate, self._new_learning_rate)
    optimizer = tf.train.GradientDescentOptimizer(FLAGS.learning_rate)
    train = optimizer.minimize(loss,
                               global_step=self.global_step,
                               gate_gradients=optimizer.GATE_NONE)
    self._train = train

  def build_eval_graph(self):
    """Build the eval graph."""
    # Eval graph

    # Each analogy task is to predict the 4th word (d) given three
    # words: a, b, c.  E.g., a=italy, b=rome, c=france, we should
    # predict d=paris.

    # The eval feeds three vectors of word ids for a, b, c, each of
    # which is of size N, where N is the number of analogies we want to
    # evaluate in one batch.
    analogy_a = tf.placeholder(dtype=tf.int32)  # [N]
    analogy_b = tf.placeholder(dtype=tf.int32)  # [N]
    analogy_c = tf.placeholder(dtype=tf.int32)  # [N]

    # Normalized word embeddings of shape [vocab_size, emb_dim].
    nemb = tf.nn.l2_normalize(self._emb, 1)

    # Each row of a_emb, b_emb, c_emb is a word's embedding vector.
    # They all have the shape [N, emb_dim]
    a_emb = tf.gather(nemb, analogy_a)  # a's embs
    b_emb = tf.gather(nemb, analogy_b)  # b's embs
    c_emb = tf.gather(nemb, analogy_c)  # c's embs

    # We expect that d's embedding vectors on the unit hyper-sphere is
    # near: c_emb + (b_emb - a_emb), which has the shape [N, emb_dim].
    target = c_emb + (b_emb - a_emb)

    # Compute cosine distance between each pair of target and vocab.
    # dist has shape [N, vocab_size].
    dist = tf.matmul(target, nemb, transpose_b=True)

    # For each question (row in dist), find the top 4 words.
    _, pred_idx = tf.nn.top_k(dist, 4)

    # Nodes for computing neighbors for a given word according to
    # their cosine distance.
    nearby_word = tf.placeholder(dtype=tf.int32)  # word id
    nearby_emb = tf.gather(nemb, nearby_word)
    nearby_dist = tf.matmul(nearby_emb, nemb, transpose_b=True)
    nearby_val, nearby_idx = tf.nn.top_k(nearby_dist,
                                         min(1000, self._options.vocab_size))

    # Nodes in the construct graph which are used by training and
    # evaluation to run/feed/fetch.
    self._analogy_a = analogy_a
    self._analogy_b = analogy_b
    self._analogy_c = analogy_c
    self._analogy_pred_idx = pred_idx
    self._nearby_word = nearby_word
    self._nearby_val = nearby_val
    self._nearby_idx = nearby_idx


  def read_data_sausage(self, para_field=2, para_field_separator='-'):
    firstRun = True
    prev_paraid=""
    data = []
    scores = []
    para = []
    para_scr = []
    with open(self._options.train_data, 'r') as f:
     for n, line in enumerate(f):
       uttid = line[:line.find(' ')]
       paraid = uttid.split(para_field_separator)[para_field-1] #uttid[:uttid.rfind('-')]
       sau = [x[1:-1].split() for x in re.findall('\[.*?\]', line)]
       wrds = [map(int, map(float,x)[::2]) for x in sau]# if x != '']
       if FLAGS.score == 'ASR':
         scr = [map(float,x)[1::2] for x in sau]
       elif FLAGS.score == 'equal':
         scr = [map(float, [float(1/len(x))]*len(x))[1::2] for x in sau]
       else:
         scr = [map(float, [float(FLAGS.score)]*len(x))[1::2] for x in sau]
       if prev_paraid == paraid:
         para.extend(wrds)
         para_scr.extend(scr)
       else:
         if firstRun:
           firstRun = False
         else:
           data.append(para)
           scores.append(para_scr)
         para = wrds
         para_scr = scr
       prev_paraid = paraid
     data.append(para)
     scores.append(para_scr)
    return data, scores

  
  def build_dataset_conf2vec(self, wordlist, dataset, scores, original_dictionary, original_reverse_dictionary, min_count, subsample):
    subsample_map = dict()
    frequency_map = dict()
    count = []
    lcv = collections.Counter(wordlist).most_common() #vocabulary_size-1)
    count.extend(lcv)
    dictionary = dict()
    drop_uniq, drop_total = 0, 0
    retain_uniq, retain_total = 0, 0
    for word, cnt in count:
      if cnt >= min_count:
        dictionary[original_reverse_dictionary[word]] = len(dictionary)
        frequency_map[original_reverse_dictionary[word]] = cnt
        retain_uniq += 1
        retain_total += cnt
      else:
        drop_uniq += 1
        drop_total += cnt
    original_unique_total = len(dictionary) + drop_uniq
    original_total = retain_total + drop_total

    if not subsample or subsample == 0:
      threshold_count = retain_total
    elif subsample < 1.0:
      threshold_count = subsample * retain_total
    else:
      raise Exception('Error: subsample value should be < 1.0. Provided - ' + str(subsample))

    downsample_uniq, downsample_total = 0, 0
    for w in dictionary:
      v = frequency_map[w]
      word_prob = (np.sqrt(v / threshold_count) + 1) * (threshold_count / v)
      if word_prob < 1.0:
        downsample_uniq += 1
        downsample_total += word_prob * v
      else:
        word_prob = 1.0
        downsample_total += v
      subsample_map[w] = int(round(word_prob * 2**32))

    print('---------------------Data Summary-----------------------')
    print('Original size of Dictionary:', original_unique_total, 'with', original_total, 'occurences')
    print('Retained dictionary entries:', retain_uniq, 'with', retain_total, 'occurences')
    print('min-count is', min_count)
    print('Words dropped from dictionary', drop_uniq)
    print('Total words dropped', drop_total)
    print('Subsample threshold:', subsample)
    print('Downsampled dictionary entries:', downsample_uniq, 'with', retain_total - downsample_total, 'occurences')
    reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))

    data, scr = [], []
    for chap_no, chap in enumerate(dataset):
      c, c_scr = [], []
      for sau_no, sau in enumerate(chap):
        s, s_scr = [], []
        for word_no, word in enumerate(sau):
          if original_reverse_dictionary[word] in dictionary and subsample_map[original_reverse_dictionary[word]] > np.random.rand() * 2 ** 32:
            s.append(dictionary[original_reverse_dictionary[word]])
            s_scr.append(scores[chap_no][sau_no][word_no])
        if len(s) != 0:
          c.append(s)
          c_scr.append(s_scr)
      if len(c) != 0:
        data.append(c)
        scr.append(c_scr)
      
    return data, scr, dictionary, reverse_dictionary, frequency_map

  def generate_batch_conf2vec(self, batch_size, num_skips, skip_window, data, scores, nWords, data_index, chap_index, word_index, o2o_center_index, o2o_target_index, mode=FLAGS.mode, loss='nce'): #, max_confusion_length=self.max_confusion_length):
    assert batch_size % num_skips == 0
    assert num_skips <= 2 * skip_window
    batch = []
    batch_scores = []
    labels = []
    label_scores = []
    span = 2 * skip_window + 1  # [ skip_window target skip_window ]
    buffer = collections.deque(maxlen=span)
    if mode == 'inter-confusion':
      index = []
      for i in range(span):
        buffer.append((data[chap_index][word_index], scores[chap_index][word_index]))
        index.append((data_index, chap_index, word_index))
        data_index = (data_index + 1) % nWords
        word_index = (word_index + 1) % len(data[chap_index])
        if data_index > 0 and word_index == 0:
          chap_index = (chap_index + 1) % len(data)
      remain_batch = batch_size
      target = flatten([buffer[i][0] for i in range(span) if i != skip_window])
      target_scores = flatten([buffer[i][1] for i in range(span) if i != skip_window])
      total_batch = len(buffer[skip_window][0][o2o_center_index:]) * len(target) - o2o_target_index
      if remain_batch < total_batch:
        data_index = index[-span][0]
        chap_index = index[-span][1]
        word_index = index[-span][2]
      while remain_batch >= total_batch:
        for n, word in enumerate(buffer[skip_window][0][o2o_center_index:]):
          batch.extend([[word]] * len(target[o2o_target_index:]))
          if FLAGS.score == '1.0':
            batch_scores.extend([[float(1)]] * len(target[o2o_target_index:]))
          else:
            batch_scores.extend([[buffer[skip_window][1][o2o_center_index + n]]] * len(target[o2o_target_index:]))
          labels.extend([j] for j in target[o2o_target_index:])
          if FLAGS.score == '1.0':
            label_scores.extend([[float(1)]] * len(target[o2o_target_index:]))
          else:
            label_scores.extend([j] for j in target_scores[o2o_target_index:])
          o2o_target_index = 0
        o2o_center_index =0
        remain_batch -= total_batch
        buffer.append((data[chap_index][word_index], scores[chap_index][word_index]))
        index.append((data_index, chap_index, word_index))
        target = flatten([buffer[i][0] for i in range(span) if i != skip_window])
        target_scores = flatten([buffer[i][1] for i in range(span) if i != skip_window])
        total_batch = len(buffer[skip_window][0][o2o_center_index:]) * len(target) - o2o_target_index
        if remain_batch >= total_batch:
          data_index = (data_index + 1) % nWords
          word_index = (word_index + 1) % len(data[chap_index])
          if data_index > 0 and word_index == 0:
            chap_index = (chap_index + 1) % len(data)
        else:
          data_index = index[-span][0]
          chap_index = index[-span][1]
          word_index = index[-span][2]
      center_end = 0
      index_end = 0
      if remain_batch != 0:
        center_end = o2o_center_index + (remain_batch + o2o_target_index) // len(target) # Calculating ending index
        index_end = (remain_batch + o2o_target_index) % len(target)
        for n, word in enumerate(buffer[skip_window][0][o2o_center_index:center_end]):
          batch.extend([[word]] * (len(target) - o2o_target_index))
          if FLAGS.score == '1.0':
            batch_scores.extend([[float(1)]] * (len(target) - o2o_target_index))
          else:
            batch_scores.extend([[buffer[skip_window][1][o2o_center_index + n]]] * (len(target) - o2o_target_index))
          labels.extend([j] for j in target[o2o_target_index:])
          if FLAGS.score == '1.0':
            label_scores.extend([[float(1)]] * len(target[o2o_target_index:]))
          else:
            label_scores.extend([j] for j in target_scores[o2o_target_index:])
          o2o_target_index = 0
        batch.extend([[buffer[skip_window][0][center_end]]] * (index_end - o2o_target_index))
        if FLAGS.score == '1.0':
          batch_scores.extend([[float(1)]] * (index_end - o2o_target_index))
        else:
          batch_scores.extend([[buffer[skip_window][1][center_end]]] * (index_end - o2o_target_index))
        labels.extend([j] for j in target[o2o_target_index:index_end])
        if FLAGS.score == '1.0':
          label_scores.extend([[float(1)]] * (index_end - o2o_target_index))
        else:
          label_scores.extend([j] for j in target_scores[o2o_target_index:index_end])
      o2o_center_index = center_end
      o2o_target_index = index_end
      padded_batch = batch
      padded_batch_scores = batch_scores
    else:
      for _ in range(span):
        buffer.append((data[chap_index][word_index], scores[chap_index][word_index]))
        data_index = (data_index + 1) % nWords
        word_index = (word_index + 1) % len(data[chap_index])
        if data_index > 0 and word_index == 0:
          chap_index = (chap_index + 1) % len(data)
      for i in range(batch_size // num_skips):
        target = skip_window  # target label at the center of the buffer
        targets_to_avoid = [skip_window]
        for j in range(num_skips):
          while target in targets_to_avoid:
            target = random.randint(0, span - 1)
          targets_to_avoid.append(target)
          input = [buffer[skip_window][0][0]] if mode == 'word2vec' else buffer[skip_window][0]
          input_scores = [1.0] if mode == 'word2vec' else buffer[skip_window][1]
          batch.append(input)
          batch_scores.append(input_scores)
          label = buffer[target][0] if mode == 'confusion' else [buffer[target][0][0]]
          label_scr = buffer[target][1] if mode == 'confusion' else [buffer[target][1][0]]
          labels.append(label)
          label_scores.append(label_scr)
          #labels[i * num_skips + j, 0] = buffer[target][0][0]
        buffer.append((data[chap_index][word_index], scores[chap_index][word_index]))
        data_index = (data_index + 1) % nWords
        word_index = (word_index + 1) % len(data[chap_index])
        if data_index > 0 and word_index == 0: # and i != (batch_size // num_skips)-1:
          chap_index = (chap_index + 1) % len(data)
      # Backtrack a little bit to avoid skipping words in the end of a batch
      data_index = (data_index + nWords - span) % nWords
      chap_index = (chap_index - 1) % len(data) if word_index - span < 0 else chap_index
      word_index = (word_index + len(data[chap_index]) - span) % len(data[chap_index])
  
      # Padding input data with zeros for fixed dimensionality
      # Get lengths of each input sample
      lens = np.array([len(batch[i]) for i in range(len(batch))])
  
      # Mask of valid places in each row
      mask = np.arange(lens.max()) < lens[:,None]
      # Setup output array and put elements from data into masked positions
      padded_batch = np.zeros(mask.shape, dtype=np.int32)
      #padded_batch = np.full(mask.shape, -1, dtype=np.int32) # Cannot use -1 since embedding_lookup fails
      padded_batch[mask] = np.hstack((batch[:]))
      padded_batch_scores = np.zeros(mask.shape, dtype=np.float64)
      padded_batch_scores[mask] = np.hstack((batch_scores[:]))
  
    # Padding for labels
    if mode == 'confusion':
      label_lens = np.array([len(labels[i]) for i in range(len(labels))])
      label_mask = np.arange(self.max_confusion_length) < label_lens[:,None] if loss == 'nce' else np.arange(label_lens.max()) < label_lens[:,None]
      padded_label = np.full(label_mask.shape, self.dummy_class, dtype=np.int32) if loss == 'nce' else np.zeros(label_mask.shape, dtype=np.int32)
      padded_label[label_mask] = np.hstack((labels[:]))
      padded_label_scores = np.zeros(label_mask.shape, dtype=np.float64)
      padded_label_scores[label_mask] = np.hstack((label_scores[:]))
    else:
      padded_label = labels
      padded_label_scores = label_scores
    return padded_batch, np.expand_dims(padded_batch_scores,-1), padded_label, np.expand_dims(padded_label_scores, -1), data_index, chap_index, word_index, o2o_center_index, o2o_target_index
  
  def generate_batch_conf2vec_confusionTraining(self, batch_size, num_skips, skip_window, data, scores, nWords, data_index, chap_index, word_index, c1, c2):
  	batch = []
  	batch_scores = []
  	labels = []
  	label_scores = []
  	total_batch = 0
  	remain_batch = int(batch_size)
  	buffer = collections.deque(maxlen=1)
  	while len(data[chap_index][word_index]) == 1:
  		data_index = (data_index + 1) % nWords
  		word_index = (word_index + 1) % len(data[chap_index])
  		if data_index > 0 and word_index == 0:
  			chap_index = (chap_index + 1) % len(data)		
  	buffer.append((data[chap_index][word_index], scores[chap_index][word_index]))
  	length = len(buffer[0][0])
  	total_batch = int(length * (length-1) - c1*(length-1) - c2) if c2 <= c1 else int(length * (length-1) - c1*(length-1) - (c2-1))
  	while remain_batch >= total_batch:
  		for n, word in enumerate(buffer[0][0][c1:], start=c1):
  			if c2 <= c1:
  				batch.extend([[word]] * (length - c2 - 1))
  				batch_scores.extend([[float(1)]] * (length - c2 - 1))
  				for word in buffer[0][0][c2:]:
  					if word == buffer[0][0][n]:
  						continue
  					labels.extend([[word]])
  					label_scores.extend([[float(1)]])
  				c2 = 0
  			else:
  				batch.extend([[word]] * (length - c2))
  				batch_scores.extend([[float(1)]] * (length - c2))		
  				for word in buffer[0][0][c2:]:
  					if word == buffer[0][0][n]:
  						continue
  					labels.extend([[word]])
  					label_scores.extend([[float(1)]])
  				c2 = 0			
  		c1 = 0
  		remain_batch -= total_batch
  		while True:
  			data_index = (data_index + 1) % nWords
  			word_index = (word_index + 1) % len(data[chap_index])
  			if data_index > 0 and word_index == 0:
  				chap_index = (chap_index + 1) % len(data)
  			if len(data[chap_index][word_index]) > 1:
  				break
  		buffer.append((data[chap_index][word_index], scores[chap_index][word_index]))
  		length = len(buffer[0][0])
  		total_batch = int(length * (length-1))
  	c1_end = 0
  	c2_end = 0
  	if remain_batch > 0:
  		end_batch = total_batch - remain_batch
  		c1_end = int((length-1) - ((end_batch - 1) // (length-1)))
  		c2_end_batch = end_batch - (length - 1 - c1_end) * (length - 1)
  		c2_end = length - c2_end_batch if length - 1 - c1_end >= c2_end_batch else length - c2_end_batch -1
  		for n, word in enumerate(buffer[0][0][c1:c1_end], start=c1):
  			if c2 <= c1:
  				batch.extend([[word]] * (length - c2 - 1))
  				batch_scores.extend([[float(1)]] * (length - c2 - 1))
  				for word in buffer[0][0][c2:]:
  					if word == buffer[0][0][n]:
  						continue
  					labels.extend([[word]])
  					label_scores.extend([[float(1)]])
  				c2 = 0
  			else:
  				batch.extend([[word]] * (length - c2))
  				batch_scores.extend([[float(1)]] * (length - c2))		
  				for word in buffer[0][0][c2:]:
  					if word == buffer[0][0][n]:
  						continue
  					labels.extend([[word]])
  					label_scores.extend([[float(1)]])		
  				c2 = 0
  		if c2_end <= c1_end:	
  			batch.extend([[buffer[0][0][c1_end]]] * c2_end)
  			batch_scores.extend([[float(1)]] * c2_end)
  			for word in buffer[0][0][:c2_end]:
  				labels.extend([[word]])
  				label_scores.extend([[float(1)]])
  		else:
  			batch.extend([[buffer[0][0][c1_end]]] * (c2_end - 1))
  			batch_scores.extend([[float(1)]] * (c2_end - 1))
  			for word in buffer[0][0][:c2_end]:
  				if word == buffer[0][0][c1_end]:
  					continue
  				labels.extend([[word]])
  				label_scores.extend([[float(1)]])		
  	padded_batch = batch
  	padded_batch_scores = batch_scores
  	padded_label = labels
  	padded_label_scores = label_scores
  	c1 = c1_end
  	c2 = c2_end
  	return padded_batch, np.expand_dims(padded_batch_scores,-1), padded_label, np.expand_dims(padded_label_scores, -1), data_index, chap_index, word_index, c1, c2


  def build_graph(self):
    """Build the graph for the full model."""
    opts = self._options
    self._examples = tf.placeholder(tf.int32, shape=[opts.batch_size, None], name="examples")
    self._example_scores = tf.placeholder(tf.float32, shape=[opts.batch_size, None, 1], name="example_scores")
    self._labels = tf.placeholder(tf.int32, shape=[opts.batch_size, self.max_confusion_length], name="labels")
    self._learning_rate = tf.Variable(opts._learning_rate, trainable=False)
    self._new_learning_rate = tf.placeholder(tf.float32, shape=[], name="new_learning_rate")
    self.pretrain_placeholder = tf.placeholder(tf.float32, shape=[opts.vocab_size, None]) #opts.emb_dim])
    #self._examples = examples
    #self._labels = labels
    true_logits, sampled_logits = self.forward(self._examples, self._example_scores, self._labels)
    loss = self.nce_loss(true_logits, sampled_logits)
    tf.summary.scalar("NCE loss", loss)
    self._loss = loss
    self._lr = FLAGS.learning_rate
    self.optimize(loss)

    # Properly initialize all variables.
    tf.global_variables_initializer().run()

    self.saver = tf.train.Saver(max_to_keep=10)


  def prepare_data(self):
    opts = self._options
    def flatten(x):
      if isinstance(x, collections.Iterable):
        return [a for i in x for a in flatten(i)]
      else:
        return [x]

    original_dictionary = dict()
    with open(FLAGS.words_file,'r') as f:
      for line in f:
        data = line.strip().split()
        original_dictionary[data[0].upper()] = int(data[1])
    original_reverse_dictionary = dict(zip(original_dictionary.values(), original_dictionary.keys()))


    if FLAGS.load_data is None:
      train_words, train_scores = self.read_data_sausage()
      train_wordlist = flatten(train_words)
      self.train_data, self.train_scores, word2id, id2word, counts = self.build_dataset_conf2vec(train_wordlist, train_words, train_scores, original_dictionary, original_reverse_dictionary, opts.min_count, opts.subsample)
      del train_words, train_scores, train_wordlist
      print('Dumping read sausage data to "sausage_data.npz"')
      np.savez('sausage_data.npz', train_data=self.train_data, train_scores=self.train_scores)
      with open('sausage_data.pkl', 'wb') as f:
        pickle.dump([word2id, id2word, counts], f)
    else:
      print('Loading', FLAGS.load_data)
      npzfile = np.load(FLAGS.load_data)
      self.train_data, self.train_scores = npzfile['train_data'], npzfile['train_scores']
      with open('sausage_data.pkl', 'rb') as f:
        word2id, id2word, counts = pickle.load(f)
    opts.words_per_epoch = len([j for i in self.train_data for j in i])
    opts.vocab_words = []
    opts.vocab_counts = []
    for idx in range(len(id2word)):
      w = id2word[idx]
      self._word2id[w.lower()] = idx
      opts.vocab_words.append(w.lower())
      opts.vocab_counts.append(counts[w])
    opts.vocab_size = len(opts.vocab_words)
    if FLAGS.mode == 'confusion':
      self.max_confusion_length = max([len(y) for x in self.train_data for y in x])
      self.dummy_class = len(id2word)
      opts.vocab_size += 1
      self._word2id['<dummy>'] = int(self.dummy_class)
      opts.vocab_words.append('<dummy>')
      opts.vocab_counts.append(-1)
      print('Max Confusion Length =', self.max_confusion_length)
      print('Adding dummy class', opts.vocab_words[-1], 'with index', self.dummy_class)
    else:
      self.max_confusion_length = 1
    print("Data file: ", opts.train_data)
    print("Vocab size: ", opts.vocab_size - 1, " + UNK")
    print("Words per epoch: ", opts.words_per_epoch)
    opts.words_to_train = opts.words_per_epoch * opts.epochs_to_train
    print('Words to train', opts.words_to_train)
    self._id2word = opts.vocab_words
    self.train_word_index, self.train_chap_index, self.train_data_index, self._epoch, self._words = 0, 0, 0, FLAGS.initial_epoch, 0
    self.train_center_index, self.train_target_index = 0, 0

  def save_vocab(self):
    """Save the vocabulary to a file so the model can be reloaded."""
    opts = self._options
    with open(os.path.join(opts.save_path, "vocab.txt"), "w") as f:
      for i in xrange(opts.vocab_size):
        vocab_word = tf.compat.as_text(opts.vocab_words[i]).encode("utf-8")
        f.write("%s %d\n" % (vocab_word,
                             opts.vocab_counts[i]))

  def _train_thread_body(self):
    opts = self._options
    breakFlag = False
    initial_epoch, epoch = self._epoch, self._epoch #self._session.run([self._epoch])
    last_words = self.train_data_index if not isinstance(self.train_data_index, dict) else max([x for x in self.train_data_index.values()])
    if FLAGS.mode == 'hybrid':
      hybrid_modes = FLAGS.hybrid_modes.strip().split(',')
      if not isinstance(self.train_data_index, dict):
        self.train_data_index = {x:0 for x in hybrid_modes}
        self.train_chap_index = {x:0 for x in hybrid_modes}
        self.train_word_index = {x:0 for x in hybrid_modes}
        self.train_center_index = {x:0 for x in hybrid_modes}
        self.train_target_index = {x:0 for x in hybrid_modes}
    while True:
      if FLAGS.mode == 'hybrid':
        results = []
        for modes in hybrid_modes:
          if modes == 'intra-confusion':
            out_results = self.generate_batch_conf2vec_confusionTraining(opts.batch_size//len(hybrid_modes), opts.window_size*2, opts.window_size, self.train_data, self.train_scores, opts.words_per_epoch, self.train_data_index[modes], self.train_chap_index[modes], self.train_word_index[modes], self.train_center_index[modes], self.train_target_index[modes])
            results.append(out_results)
          else:
            out_results = self.generate_batch_conf2vec(opts.batch_size//len(hybrid_modes), opts.window_size*2, opts.window_size, self.train_data, self.train_scores, opts.words_per_epoch, self.train_data_index[modes], self.train_chap_index[modes], self.train_word_index[modes], self.train_center_index[modes], self.train_target_index[modes], mode=modes)
            results.append(out_results)
          self.train_data_index[modes] = out_results[4]
          self.train_chap_index[modes] = out_results[5]
          self.train_word_index[modes] = out_results[6]
          self.train_center_index[modes] = out_results[7]
          self.train_target_index[modes] = out_results[8]
        batch_inputs = np.concatenate([x[0] for x in results], axis=0)
        batch_scores = np.concatenate([x[1] for x in results], axis=0)
        batch_labels = np.concatenate([x[2] for x in results], axis=0)
      elif FLAGS.mode == 'intra-confusion':
        batch_inputs, batch_scores, batch_labels, _, self.train_data_index, self.train_chap_index, self.train_word_index, self.train_center_index, self.train_target_index = self.generate_batch_conf2vec_confusionTraining(opts.batch_size, opts.window_size*2, opts.window_size, self.train_data, self.train_scores, opts.words_per_epoch, self.train_data_index, self.train_chap_index, self.train_word_index, self.train_center_index, self.train_target_index)
      else:
        batch_inputs, batch_scores, batch_labels, _, self.train_data_index, self.train_chap_index, self.train_word_index, self.train_center_index, self.train_target_index = self.generate_batch_conf2vec(opts.batch_size, opts.window_size*2, opts.window_size, self.train_data, self.train_scores, opts.words_per_epoch, self.train_data_index, self.train_chap_index, self.train_word_index, self.train_center_index, self.train_target_index)
      self.feed_dict = {self._examples: batch_inputs, self._example_scores: batch_scores, self._labels: batch_labels, self._new_learning_rate: self._lr}
      train_data_index = self.train_data_index if not isinstance(self.train_data_index, dict) else max([x for x in self.train_data_index.values()])
      _ = self._session.run([self._train], feed_dict=self.feed_dict)
      if last_words > train_data_index: 
        breakFlag = True
      last_words = train_data_index
      if breakFlag:
        break

  def train(self):
    """Train the model."""
    opts = self._options
    breakFlag = False
    initial_words = 0
    summary_op = tf.summary.merge_all()
    summary_writer = tf.summary.FileWriter(opts.save_path, self._session.graph)
    workers = []
    for _ in xrange(opts.concurrent_steps):
      t = threading.Thread(target=self._train_thread_body)
      t.start()
      workers.append(t)

    last_words, last_time, last_summary_time = initial_words, time.time(), 0
    while True:
      time.sleep(opts.statistics_interval)  # Reports our progress once a while.
      step, loss = self._session.run([self.global_step, self._loss], feed_dict=self.feed_dict)

      if not FLAGS.disable_lr_scheduler:
        lr_ratio_completed = self._words / float(opts.words_to_train)
        lr_ratio_remaining = 1.0 - lr_ratio_completed
        self._lr = opts._learning_rate * max(0.0001, lr_ratio_remaining)
        self._session.run(self.lr_op, feed_dict=self.feed_dict)
      
      now = time.time()
      train_data_index = self.train_data_index if not isinstance(self.train_data_index, dict) else max([x for x in self.train_data_index.values()])
      if last_words > train_data_index: 
        last_words, last_time, rate = train_data_index, now, (train_data_index + opts.words_per_epoch - last_words) / (now - last_time)
        print("Epoch %4d Step %8d: lr = %5.3f loss = %6.2f words/sec = %8.0f words_processed = %8d\r" %
            (self._epoch, step, self._learning_rate.eval(), loss, rate, self._words), end="") #train_data_index), end="")
        breakFlag = True
      else:
        last_words, last_time, rate = train_data_index, now, (train_data_index - last_words) / (now - last_time)
        print("Epoch %4d Step %8d: lr = %5.3f loss = %6.2f words/sec = %8.0f words_processed = %8d\r" %
            (self._epoch, step, self._learning_rate.eval(), loss, rate, self._words), end="") #self.train_data_index), end="")
      sys.stdout.flush()
      self._words = ((self._epoch-1)*opts.words_per_epoch)+train_data_index
      if now - last_summary_time > opts.summary_interval:
        summary_str = self._session.run(summary_op, feed_dict=self.feed_dict)
        summary_writer.add_summary(summary_str, step)
        last_summary_time = now
      if breakFlag:
        break
    if self._epoch - self._last_checkpoint_time >= opts.checkpoint_interval:
      self.saver.save(self._session,
                      os.path.join(opts.save_path, "model.ckpt"),
                      global_step=int(self._epoch))
      self._last_checkpoint_time = self._epoch

    for t in workers:
      t.join()

    self._epoch += 1
    return

  def _predict(self, analogy):
    """Predict the top 4 answers for analogy questions."""
    idx, = self._session.run([self._analogy_pred_idx], {
        self._analogy_a: analogy[:, 0],
        self._analogy_b: analogy[:, 1],
        self._analogy_c: analogy[:, 2]
    })
    return idx

  def eval(self, analogy_questions):
    """Evaluate analogy questions and reports accuracy."""

    # How many questions we get right at precision@1.
    correct = 0

    try:
      total = analogy_questions.shape[0]
      #total = self._analogy_questions.shape[0]
    except AttributeError as e:
      raise AttributeError("Need to read analogy questions.")

    start = 0
    while start < total:
      limit = start + 2500
      sub = analogy_questions[start:limit, :]
      #sub = self._analogy_questions[start:limit, :]
      idx = self._predict(sub)
      start = limit
      for question in xrange(sub.shape[0]):
        for j in xrange(4):
          if idx[question, j] == sub[question, 3]:
            # Bingo! We predicted correctly. E.g., [italy, rome, france, paris].
            correct += 1
            break
          elif idx[question, j] in sub[question, :3]:
            # We need to skip words already in the question.
            continue
          else:
            # The correct label is not the precision@1
            break
    print()
    print("Eval %4d/%d accuracy = %4.1f%%" % (correct, total,
                                              correct * 100.0 / total))

  def analogy(self, w0, w1, w2):
    """Predict word w3 as in w0:w1 vs w2:w3."""
    wid = np.array([[self._word2id.get(w, 0) for w in [w0, w1, w2]]])
    idx = self._predict(wid)
    for c in [self._id2word[i] for i in idx[0, :]]:
      if c not in [w0, w1, w2]:
        print(c)
        return
    print("unknown")

  def nearby(self, words, num=20):
    """Prints out nearby words given a list of words."""
    ids = np.array([self._word2id.get(x, 0) for x in words])
    vals, idx = self._session.run(
        [self._nearby_val, self._nearby_idx], {self._nearby_word: ids})
    for i in xrange(len(words)):
      print("\n%s\n=====================================" % (words[i]))
      for (neighbor, distance) in zip(idx[i, :num], vals[i, :num]):
        print("%-20s %6.4f" % (self._id2word[neighbor], distance))


def _start_shell(local_ns=None):
  # An interactive shell is useful for debugging/development.
  import IPython
  user_ns = {}
  if local_ns:
    user_ns.update(local_ns)
  user_ns.update(globals())
  IPython.start_ipython(argv=[], user_ns=user_ns)

def initialize_model(opts, session):
  pretrainings = [x for x in [(FLAGS.fixed_embed_pretrain,True), (FLAGS.pretrain,False)] if x[0] is not None]
  pretrain_models = []
  softmax_models = []
  for pretrain_filename, fixed in pretrainings: 
    if os.path.isfile(pretrain_filename):
      if FLAGS.initialize_softmax:
        print('ERROR: Cannot initialize GoogleNews W2V Softmax - No weights available')
        sys.exit(1)
      with open(pretrain_filename, 'r') as f:
        header = unicode(f.readline(), 'utf-8')
        emb_dim = int(header.strip().split()[1])
      from gensim.models.keyedvectors import KeyedVectors
      if fixed:
        w2v = KeyedVectors.load_word2vec_format(FLAGS.fixed_embed_pretrain, binary=not FLAGS.fixed_embed_pretrain_text)
      else:
        w2v = KeyedVectors.load_word2vec_format(FLAGS.pretrain, binary=not FLAGS.pretrain_text)
      print("Setting embedding_size of", pretrain_filename, emb_dim)
      pretrain_models.append(w2v)
    else:
      modelpath = tf.train.latest_checkpoint(FLAGS.fixed_embed_pretrain) if fixed else tf.train.latest_checkpoint(FLAGS.pretrain)
      g = tf.Graph()
      with tf.Session(graph=g) as sess:
        saver = tf.train.import_meta_graph(modelpath + '.meta')
        saver.restore(sess, os.path.abspath(modelpath))
        graph = tf.get_default_graph()
        if FLAGS.pretrain_embedding_name is not None:
          if FLAGS.pretrain_embedding_name not in [x.name for x in tf.trainable_variables()]:
            print('Embedding name not found in the checkpoint model:',[x.name for x in tf.trainable_variables()])
            sys.exit(1)
        varName = FLAGS.pretrain_embedding_name
        w2v_embeddings = graph.get_tensor_by_name(varName).eval()
        emb_dim = w2v_embeddings.shape[1] 
        pretrain_models.append(w2v_embeddings)
        if FLAGS.initialize_softmax:
          softmax_models.append((graph.get_tensor_by_name('sm_w_t:0').eval(), graph.get_tensor_by_name('sm_b:0').eval()))
        print("Setting embedding_size of", pretrain_filename, emb_dim)
    
    if fixed:
      opts.fixed_emb_dim = emb_dim
    else:
      opts.emb_dim = emb_dim

  model = Word2Vec(opts, session)
  opts = model._options
  softmax_weights = []
  for n, pretrain_model in enumerate(pretrain_models):
    print("Initializing Network with pre-trained model: "+pretrainings[n][0])
    emb_dim = opts.fixed_emb_dim if pretrainings[n][1] else opts.emb_dim
    if not isinstance(pretrain_model, np.ndarray):
      w2v_embeddings = np.empty((opts.vocab_size, emb_dim))
      indices, not_indices = replace_apostrophe_index(pretrain_model, opts.vocab_size, opts.vocab_words) #, train_count)
      if FLAGS.mode == 'confusion':
        not_indices.append(opts.vocab_size - 1)
      print("# of vectors loaded from pretrained word2vec model:", len(indices), "/", opts.vocab_size)  #len(train_count))
      print("# of vectors initialized randomly:", len(not_indices), "/", opts.vocab_size)  #len(train_count))
      w2v_embeddings[[x[0] for x in indices],:] = pretrain_model.syn0[[x[1] for x in indices],:]
      w2v_embeddings[not_indices,:] = np.random.uniform(-1.0, 1.0, size=(len(not_indices),emb_dim))
    else:
      pretrain_vocab = {}
      with open(pretrainings[n][0]+'/vocab.txt', 'r') as f:
        for idx, line in enumerate(f):
          pretrain_vocab[line.strip().split()[0]] = idx 
      #w2v_embeddings = np.empty((opts.vocab_size, emb_dim))
      indices = [pretrain_vocab[x] for x in opts.vocab_words]
      w2v_embeddings = pretrain_model[indices,:]
      if FLAGS.initialize_softmax:
        softmax_weights.append((softmax_models[n][0][indices,:],softmax_models[n][1][indices]))
    if FLAGS.joint_mode:
      if 'concatenate_list' in locals():
        concatenate_list.append(w2v_embeddings)
      else:
        concatenate_list = [w2v_embeddings]
    elif pretrainings[n][1]:
      session.run(model.pretrain_fixed, feed_dict={model.pretrain_placeholder: w2v_embeddings})
    else:
      session.run(model.pretrain, feed_dict={model.pretrain_placeholder: w2v_embeddings})
  if FLAGS.joint_mode:
    assert concatenate_list[0].shape[0] == concatenate_list[1].shape[0], "Conflicting vocabulary sizes between two model embeddings"
    w2v_embeddings = np.concatenate(concatenate_list, axis=1)
    session.run(model.pretrain, feed_dict={model.pretrain_placeholder: w2v_embeddings})
  if FLAGS.initialize_softmax:
    sm_wt = np.concatenate([x[0] for x in softmax_weights], axis=1)
    session.run(model.initialize_sm_w_t, feed_dict={model.pretrain_placeholder: sm_wt})
    session.run(model.initialize_sm_b, feed_dict={model.pretrain_placeholder: np.expand_dims(softmax_weights[0][1],-1)})
  return model

def main(_):
  """Train a word2vec model."""
  if not FLAGS.train_data or not FLAGS.eval_data or not FLAGS.save_path or not FLAGS.words_file:
    print("--train_data --eval_data and --save_path must be specified.")
    sys.exit(1)
  opts = Options()
  with tf.Graph().as_default(), tf.Session() as session:
    with tf.device("/cpu:0"):
      model = initialize_model(opts, session)
      _analogy_questions = model.read_analogies(FLAGS.eval_data) # Read analogy questions
      # Read confusion analogy questions
      _conf_analogy_questions = model.read_analogies(FLAGS.conf_eval_data) if FLAGS.conf_eval_data is not None else None
    if FLAGS.restore is None:
      restore_file = tf.train.latest_checkpoint(opts.save_path)
      if restore_file is not None:
        print('Found checkpoints in ' + opts.save_path + '. Loading ' + restore_file)
        model.saver.restore(session, restore_file)
    else:
      restore_ext = FLAGS.restore[FLAGS.restore.rfind('.')+1:FLAGS.restore.rfind('-')]
      restore_file = FLAGS.restore if restore_ext not in ['meta', 'index', 'data'] else FLAGS.restore[:FLAGS.restore.rfind('.')+1:] 
      print('Loading checkpoint ' + restore_file)
      model.saver.restore(session, restore_file)
    model._last_checkpoint_time = 0
    model.eval(_analogy_questions)  # Eval analogies.
    if FLAGS.conf_eval_data is not None:
      model.eval(_conf_analogy_questions)  # Eval analogies.
    for _ in xrange(FLAGS.initial_epoch-1, opts.epochs_to_train):
      model.train()  # Process one epoch
      model.eval(_analogy_questions)  # Eval analogies.
      if FLAGS.conf_eval_data is not None:
        model.eval(_conf_analogy_questions)  # Eval analogies.
    # Perform a final save.
    model.saver.save(session,
                     os.path.join(opts.save_path, "model.ckpt"),
                     global_step=None)
    if FLAGS.interactive:
      # E.g.,
      # [0]: model.analogy(b'france', b'paris', b'russia')
      # [1]: model.nearby([b'proton', b'elephant', b'maxwell'])
      _start_shell(locals())


if __name__ == "__main__":
  tf.app.run()
