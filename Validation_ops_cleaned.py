# Written by Prashanth Gurunath Shivakumar
# email: pgurunat@usc.edu
# University of Southern California
#
# example usage:
#
# python Validation_ops.py --conf2vec-epoch 'latest' --word2vec-epoch 'latest' --metadata-file model1/vocab.txt --embedding-name-word2vec emb:0 --embedding-name-conf2vec emb:0 --analogies questions-words.txt --homophone-analogies homophone_analogy.txt --homophone-ratings homophone_ratings.txt --human-ratings wordsim353.tsv baseline_word2vec_model confusion2vec_model
#


from __future__ import print_function

import math
import os,sys
import argparse

import scipy
from scipy import stats
import numpy as np
from collections import defaultdict
from gensim.models.keyedvectors import KeyedVectors
from gensim.models import word2vec
import tensorflow as tf 
from tensorflow.python.tools.inspect_checkpoint import print_tensors_in_checkpoint_file
import time
from collections import OrderedDict
import nltk
from nltk.corpus import wordnet
from nltk.metrics import edit_distance
import itertools
import pickle
import multiprocessing
from functools import partial
from gensim import matutils, utils
from sklearn.decomposition import PCA

os.environ['CUDA_VISIBLE_DEVICES'] = '' #Run on CPU

parser = argparse.ArgumentParser(description='Comparing different conf2vec models with original word2vec')
parser.add_argument('--embedding-name-word2vec', default=None, type=str, help="Name of the embedding to use for loading Word2Vec model")
parser.add_argument('--embedding-name-conf2vec', default=None, type=str, help="Name of the embedding to use for loading Conf2Vec model")
parser.add_argument('--fixed-emb-name', default=None, type=str, help="Name of the fixed embedding to use for loading Conf2Vec model (relevant only with --branched-mode")
parser.add_argument('--metadata-file', default=None, type=str, help="Override path to metadata.tsv file")
parser.add_argument('--topk', default=None, type=str, help="Top k words for comparision; For multiple runs use comma-separated (Ex: '100,200,500')")
parser.add_argument('--restrict-vocab', default=None, type=str, help="Restrict vocabulary to Top k words; For multiple runs use comma-separated (Ex: '100,200,500')")
parser.add_argument('--word2vec-epoch', default='best', type=str, help="Choose specific model (Word2Vec) [epoch or 'best' or 'latest']")
parser.add_argument('--conf2vec-epoch', default='best', type=str, help="Choose specific model (Conf2Vec) [epoch or 'best' or 'latest']")
parser.add_argument('--conf-topk', default=10, type=int, help="Top k words for confusion comparison")
parser.add_argument('--conf-restrict-vocab', default=None, type=str, help="Restrict vocabulary to Top k words for confusion comparison")
parser.add_argument('--synonym-dict-path', default=None, type=str, help="Help to pickle file containing synonym dictionary")
parser.add_argument('--threads', default=16, type=int, help="# of threads to use for multiprocessing")
parser.add_argument('--prefix-plots', default='', type=str, help="Prefix for plots directory")
parser.add_argument('--dump-vocab', type=str, default=None, help="Path of output pickle file for dumping vocab")
parser.add_argument('--vocab-path', default=None, type=str, help="Path to pickle file vocab (OrderedDict)")
parser.add_argument('--concatenate', action='store_true', help="Concatenate the model with the reference word2vec")
parser.add_argument('--dump-concatenated-model', action='store_true', help="Dump the concatenated model in W2V text format")
parser.add_argument('--analogies', default=None, type=str, help="Path to analogies questions.txt")
parser.add_argument('--analogies-restrict-vocab', default=None, type=int, help="Reduce vocabulary of the model for fast approximate evaluation")
parser.add_argument('--analogies-case-sensitive', action='store_true', help="Take into account the case in analogies file")
parser.add_argument('--analogies-original-word2vec', action='store_true', help="Calculate analogies for original word2vec model")
parser.add_argument('--homophone-analogies', default=None, type=str, help="Path to homophone analogies file")
parser.add_argument('--homophone-ratings', default=None, type=str, help="Path to homophone ratings file")
parser.add_argument('--human-ratings', default=None, type=str, help="Path to human-ratings file (wordsim353.tsv)")
parser.add_argument('--human-ratings-delimiter', default='\t', type=str, help="Delimiter to use while parsing human-ratings file")
parser.add_argument('--human-ratings-case-sensitive', action='store_true', help="Take into account the case in human ratings file")
parser.add_argument('--human-ratings-original-word2vec', action='store_true', help="Calculate human-ratings correlations for original word2vec model")
parser.add_argument('--apply-pca', action='store_true', help="Apply PCA before evaluation")
parser.add_argument('--pca-whiten', action='store_true', help="Uncorrelate the output (PCA)")
parser.add_argument('--tsne-wordlist', default=None, type=str, help="List of words to be plotted using TSNE (one word per line format.")
parser.add_argument('--filter-w2v-vocab', action='store_true', help="Filter the vocabulary to contain the intersection of two models. If false, random embeddings for missing words in w2v")
parser.add_argument('--branched-mode', action='store_true', help="Evaluation for branched mode")

parser.add_argument('WORD2VEC_MODEL', help="Path to word2vec model")
parser.add_argument('MODELDIR', type=str, help="Path to the model directory")

args = parser.parse_args()
print(args)

if args.metadata_file is None:
  vocabFile = args.MODELDIR + '/metadata.tsv'
else:
  vocabFile = args.metadata_file

if not os.path.exists(vocabFile):
  print('metadata.tsv File not found in ' + args.MODELDIR + '. Exiting...')
  sys.exit(1)

if not os.path.exists(args.MODELDIR+'/'+args.prefix_plots+'_plots'):
  os.makedirs(args.MODELDIR+'/'+args.prefix_plots+'_plots')

if args.topk is not None and ',' in args.topk:
  topK_list = [x for x in sorted([int(x) for x in args.topk.split(',')], reverse=True)]
  #topK = topK_list.pop(0)
elif args.topk is None:
  topK = None
  topK_list = [None]
else:
  topK = int(args.topk)
  topK_list = [int(args.topk)]

if args.restrict_vocab is not None and ',' in args.restrict_vocab:
  vocabK_list = sorted([int(x) for x in args.restrict_vocab.split(',')], reverse=True)
  vocabSz = vocabK_list[0]
elif args.restrict_vocab is not None:
  vocabK_list = [int(args.restrict_vocab)]
  vocabSz = int(args.restrict_vocab)
else:
  vocabSz = None
  vocabK_list = [None]

if args.conf_restrict_vocab is not None and ',' in args.conf_restrict_vocab:
  conf_vocabK_list = sorted([int(x) for x in args.conf_restrict_vocab.split(',')], reverse=True)
  conf_vocabSz = conf_vocabK_list[0]
elif args.conf_restrict_vocab is not None:
  conf_vocabK_list = [int(args.conf_restrict_vocab)]
  conf_vocabSz = int(args.conf_restrict_vocab)
else:
  conf_vocabSz = None
  conf_vocabK_list = [None]

c2v_model = args.MODELDIR + '/model.ckpt-' + args.conf2vec_epoch
if args.conf2vec_epoch == 'latest':
  c2v_model = tf.train.latest_checkpoint(args.MODELDIR)
  if c2v_model is None:
    print('No checkpoint candidates found. Exiting...')
    sys.exit(1)
  else:
    print('Loading latest checkpoint: ' +  c2v_model)
elif args.conf2vec_epoch != 'best' and not os.path.exists(c2v_model + '.meta'):
  print(c2v_model + ' not found! Exiting...')
  sys.exit(1)
elif args.conf2vec_epoch == 'best' and not os.path.exists(args.MODELDIR + '/best_model'):
  print(args.MODELDIR + '/best_model not found! Exiting...')
  sys.exit(1)
else:
  with open(args.MODELDIR + '/best_model', 'r') as f:
    best_model = f.readlines()[0].strip() 
  c2v_model = args.MODELDIR + '/model.ckpt-' + best_model
  if not os.path.exists(c2v_model + '.meta'):
    print(c2v_model + ' not found! Exiting...')
    sys.exit(1)

conf2vec_vocab={}
with open(vocabFile, 'r') as f:
  n_ = 0
  for n, line in enumerate(f):
    if 'Word\tFrequency' in line:
      continue
    conf2vec_vocab[line.split()[0].lower()] = n_
    n_ += 1


def phone_edit_distance(ref, hyp):
  return min([edit_distance(x,y)/float(max(len(x),len(y))) for x,y in itertools.product(ref, hyp)])

def get_tf_embeddings(modelpath, filterfunc=None, normalize=True, conf2vec_flag=True, branched=False):
  g = tf.Graph()
  with tf.Session(graph=g) as sess:
    saver = tf.train.import_meta_graph(modelpath + '.meta')
    saver.restore(sess, os.path.abspath(modelpath))
    graph = tf.get_default_graph()
    if conf2vec_flag and args.embedding_name_conf2vec is not None:
      if args.embedding_name_conf2vec not in [x.name for x in tf.trainable_variables()]:
        print('Embedding name not found in the checkpoint model:',[x.name for x in tf.trainable_variables()])
        sys.exit(1)
      fixedvarName = 'fixed_emb:0' if args.fixed_emb_name is None else args.fixed_emb_name
      if branched and fixedvarName not in [x.name for x in tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES)]:
        print('--branched-mode provided but no variable of name "fixed_emb:0" found in '+ args.MODELDIR + '. Please make sure the branched model corresponds to the MODELDIR.')
        sys.exit(1)
      elif branched:
        fixed_embeddings = graph.get_tensor_by_name(fixedvarName).eval()
      varName = args.embedding_name_conf2vec
    elif not conf2vec_flag and args.embedding_name_word2vec is not None:
      if args.embedding_name_word2vec not in [x.name for x in tf.trainable_variables()]:
        print('Embedding name not found in the checkpoint model')
        sys.exit(1)
      varName = args.embedding_name_word2vec
    else:
      varName = "embedding/Confusion_Embeddings:0" if "embedding/Confusion_Embeddings:0" in [x.name for x in tf.trainable_variables()] else "Variable_1:0"
    try:
      embeddings = graph.get_tensor_by_name(varName).eval()
    except KeyError:
      print('ERROR: Variable name ' + varName + ' not found! Possible candidates: ' + ' '.join([x.name for x in tf.trainable_variables()]))
      sys.exit(1)
    if branched:
      embeddings = np.concatenate((fixed_embeddings, embeddings), axis=1)
    if normalize:
      embeddings = ( embeddings / np.sqrt((embeddings ** 2).sum(-1))[..., np.newaxis]).astype(np.float32)
  return embeddings if filterfunc is None else embeddings[filterfunc,:]

def analogy_answer(predicted, label, idx2word, cmudict, mode, thres):
  if mode == "nearest":
    return predicted[0] == label
  elif mode == "edit_dist":
    if idx2word[label] in cmudict and idx2word[predicted[0]] in cmudict:
      return phone_edit_distance(cmudict[idx2word[label]], cmudict[idx2word[predicted[0]]]) <= thres
    else:
      return predicted[0] == label
  elif mode == "topk":
    for predict in predicted:
      if predict == label:
        return True
    return False


def read_and_score_analogies(word2idx, idx2word, emb, k=3, filename=args.analogies, case_insensitive=True, mode="nearest", thres=0.25, dump=False):
  cmudict = nltk.corpus.cmudict.dict()
  sections, section = [], None
  answers, correct = [], 0
  skipped = 0
  with open(filename,'r') as f:
    for line_no, line in enumerate(f):
      line = utils.to_unicode(line)
      if line.startswith(': '):
        if section:
          sections.append(section)
        section = {'section':line.lstrip(': ').strip(), 'correct':[], 'incorrect':[]}
      else:
        if not section:
          raise ValueError("missing section header before line #%i in %s" % (line_no, filename))
        try:
          if case_insensitive:
            a, b, c, expected = [word.lower() for word in line.split()]
          else:
            a, b, c, expected = line.split()
        except ValueError:
          skipped += 1
          continue

        if a not in word2idx or b not in word2idx or c not in word2idx or expected not in word2idx:
          skipped += 1
          continue

        ignore = {a, b, c}
        predicted = None
        emb_a = emb[word2idx[a],:]
        emb_b = emb[word2idx[b],:]
        emb_c = emb[word2idx[c],:]
        emb_d = emb_c + (emb_b - emb_a)
        emb_d = ( emb_d / np.sqrt((emb_d ** 2).sum(-1))[..., np.newaxis]).astype(np.float32)
        sim = np.dot(emb, emb_d.T)
        topk = matutils.argsort(sim, reverse=True)[:k+3]
        mask_abc = np.in1d(topk, [word2idx[x] for x in ignore])
        masked = topk[~mask_abc]
        answer = analogy_answer(masked[:k], word2idx[expected], idx2word, cmudict, mode, thres)
        correct += answer
        answers.append(answer)
        section['correct' if answer else 'incorrect'].append((a, b, c, expected, idx2word[masked[0]]))
    if section:
      sections.append(section)

    if filename == args.analogies:
      semantic = {'section':'semantic', 'correct':sum((s['correct'] for s in sections if 'gram' not in s['section']), []), 'incorrect':sum((s['incorrect'] for s in sections if 'gram' not in s['section']), [])}
      syntactic = {'section':'syntactic', 'correct':sum((s['correct'] for s in sections if 'gram' in s['section']), []), 'incorrect':sum((s['incorrect'] for s in sections if 'gram' in s['section']), [])}
      sections.append(semantic)
      sections.append(syntactic)
    else:
      typeI = {'section':'type-I', 'correct':sum((s['correct'] for s in sections if 'type-II' not in s['section']), []), 'incorrect':sum((s['incorrect'] for s in sections if 'type-II' not in s['section']), [])}
      typeII = {'section':'type-II', 'correct':sum((s['correct'] for s in sections if 'type-II' in s['section']), []), 'incorrect':sum((s['incorrect'] for s in sections if 'type-II' in s['section']), [])}
      syntactic = {'section':'syntactic', 'correct':sum((s['correct'] for s in sections if 'type-II' in s['section'] and 'gram' in s['section']), []), 'incorrect':sum((s['incorrect'] for s in sections if 'type-II' in s['section'] and 'gram' in s['section']), [])}
      semantic = {'section':'semantic', 'correct':sum((s['correct'] for s in sections if 'type-II' in s['section'] and 'gram' not in s['section']), []), 'incorrect':sum((s['incorrect'] for s in sections if 'type-II' in s['section'] and 'gram' not in s['section']), [])}
      sections.append(semantic)
      sections.append(syntactic)
      sections.append(typeI)
      sections.append(typeII)

    total = {'section':'total', 'correct':sum((s['correct'] for s in sections[-2:]), []), 'incorrect':sum((s['incorrect'] for s in sections[-2:]), [])}
    sections.append(total)
  num_analogies = len(sections[-1]['correct'])+len(sections[-1]['incorrect'])

  print("Analogy Results:")
  print("Total Analogies Read:", num_analogies + skipped) 
  print("No of analogies skipped:", skipped)
  print("Total Analogies Scored:", num_analogies)
  print("Total Num Correct:", len(sections[-1]['correct']))
  print("Total Accuracy:", float(len(sections[-1]['correct']))/(len(sections[-1]['correct'])+len(sections[-1]['incorrect'])))
  analogy_type = "Semantic Analogies:" if filename == args.analogies else "Homophone Type-I"
  print("Num " + analogy_type, len(sections[-3]['correct'])+len(sections[-3]['incorrect']))
  print(analogy_type + " Correct:", len(sections[-3]['correct']))
  print(analogy_type + " Accuracy:", float(len(sections[-3]['correct']))/(len(sections[-3]['correct'])+len(sections[-3]['incorrect'])))
  analogy_type = "Syntactic Analogies:" if filename == args.analogies else "Homophone Type-II"
  print("Num " + analogy_type, len(sections[-2]['correct'])+len(sections[-2]['incorrect']))
  print(analogy_type + " Correct:", len(sections[-2]['correct']))
  print(analogy_type + " Accuracy:", float(len(sections[-2]['correct']))/(len(sections[-2]['correct'])+len(sections[-2]['incorrect'])))
  if filename != args.analogies:
    analogy_type = "Homophone Type-II Semantic Analogies:"
    print("Num " + analogy_type, len(sections[-5]['correct'])+len(sections[-5]['incorrect']))
    print(analogy_type + " Correct:", len(sections[-5]['correct']))
    print(analogy_type + " Accuracy:", float(len(sections[-5]['correct']))/(len(sections[-5]['correct'])+len(sections[-5]['incorrect'])))
    analogy_type = "Homophone Type-II Syntactic Analogies:"
    print("Num " + analogy_type, len(sections[-4]['correct'])+len(sections[-4]['incorrect']))
    print(analogy_type + " Correct:", len(sections[-4]['correct']))
    print(analogy_type + " Accuracy:", float(len(sections[-4]['correct']))/(len(sections[-4]['correct'])+len(sections[-4]['incorrect'])))
  if dump:
    with open('analogy.dump', 'w') as f:
      analogy_type = "Semantic Analogies:" if filename == args.analogies else "Homophone Type-I"
      f.write(': '+analogy_type + '-Correct' + os.linesep)
      for analogy in sections[-3]['correct']:        
        f.write(str(analogy)+os.linesep)
      f.write(': '+analogy_type + '-Incorrect' + os.linesep)
      for analogy in sections[-3]['incorrect']:        
        f.write(str(analogy)+os.linesep)
      analogy_type = "Syntactic Analogies:" if filename == args.analogies else "Homophone Type-II"
      f.write(': '+analogy_type + '-Correct'+os.linesep)
      for analogy in sections[-2]['correct']:        
        f.write(str(analogy) + os.linesep)
      f.write(': '+analogy_type + '-Incorrect' + os.linesep)
      for analogy in sections[-2]['incorrect']:        
        f.write(str(analogy)+os.linesep)




def score_human_correlation(word2idx, emb, filename=args.human_ratings, delimiter=args.human_ratings_delimiter, dummy4unknown=False, case_insensitive=True):
  oov = 0
  similarity_gold = []
  similarity_model = []

  with open(filename, 'r') as f:
    for idx, line in enumerate(f):
      if line.startswith('#'):
        continue
      else:
        try:
          a, b, sim = [x.lower() if case_insensitive else x for x in line.split(delimiter)]
          sim = float(sim)
        except:
          print('skipping invalid line', line)
          continue

        if a not in word2idx or b not in word2idx:
          oov += 1
          if dummy4unknown:
            similarity_model.append(0.0)
            similarity_gold.append(sim)
          continue
        
        emb_a = emb[word2idx[a],:]
        emb_b = emb[word2idx[b],:]
        model_sim = np.dot(emb_a, emb_b.T)
        similarity_model.append(model_sim)
        similarity_gold.append(sim)

  spearman, s_pval = scipy.stats.spearmanr(similarity_gold, similarity_model)
  pearson, p_pval = scipy.stats.pearsonr(similarity_gold, similarity_model)
  oov_ratio = float(oov)/(len(similarity_gold) + oov) * 100

  print("Correlation with human ratings:")
  print("Pearson correlation:", pearson, "with p-value:", p_pval)
  print("Spearman correlation:", spearman, "with p-value:", s_pval)
  print("OOV:", oov_ratio)


def plot_tsne(embs, label_dict, file_prefix):
  from sklearn.manifold import TSNE
  import matplotlib
  matplotlib.use('Agg')
  import matplotlib.pyplot as plt

  tsne = TSNE(perplexity=30, n_components=2, init='pca', n_iter=5000)
  if args.tsne_wordlist is None:
    plot_only = 100
    low_dim_embs = tsne.fit_transform(embs[:plot_only, :])
    labels = label_dict.keys()[:plot_only]
  else:
    with open(args.tsne_wordlist, 'r') as f:
      labels, plot_only = [], []
      for line in f:
        labels.append(line.strip())
        plot_only.append(label_dict[line.strip()])
    print(labels, plot_only, [label_dict[x] for x in labels])
    low_dim_embs = tsne.fit_transform(embs[plot_only, :])
  assert low_dim_embs.shape[0] >= len(labels), "More labels than embeddings"
  plt.figure(figsize=(18, 18))  # in inches
  for i, label in enumerate(labels):
    x, y = low_dim_embs[i, :]
    plt.scatter(x, y)
    plt.annotate(label,
                 xy=(x, y),
                 xytext=(5, 2),
                 textcoords='offset points',
                 ha='right',
                 va='bottom')
  filename = args.MODELDIR+'/'+args.prefix_plots+'_plots/' + file_prefix + '_tsne.png'
  plt.savefig(filename, format='png', dpi=400)

if args.vocab_path is not None:
  with open(args.vocab_path,'rb') as f:
    vocabMap = pickle.load(f)

if os.path.isfile(args.WORD2VEC_MODEL):
  print('Loading word2vec model...')
  w2v_model = KeyedVectors.load_word2vec_format(args.WORD2VEC_MODEL, binary=True)
  w2v_model.init_sims()
  vocab = set(conf2vec_vocab.keys()) & set(w2v_model.vocab.keys())
  if args.filter_w2v_vocab:
    vocabMap = OrderedDict(sorted([x for x in conf2vec_vocab.items() if x[0] in vocab], key=lambda t: t[1])) if args.vocab_path is None else vocabMap
    w2v_idx = [w2v_model.vocab[x].index for x in vocabMap.keys()]
    w2v_embeddings = w2v_model.syn0norm[w2v_idx,:]
  else:
    vocabMap = OrderedDict(sorted(conf2vec_vocab.items(), key=lambda t: t[1])) if args.vocab_path is None else vocabMap
    if '<dummy>' in vocabMap:
      del vocabMap['<dummy>'] 
    try:
      w2v_idx = [w2v_model.vocab[x].index for x in vocabMap.keys()]
    except KeyError:
      print('Error: The vocabulary of the two models do not match! Try using --filter-w2v-vocab flag to compute the evaluations on the intersection of the two vocabularies OR use --vocab-path to enforce a common vocabulary')
      sys.exit(1)
    w2v_embeddings = w2v_model.syn0norm[w2v_idx,:]

if args.dump_vocab:
  print('Dumping vocab to vocab.pkl')
  with open(args.dump_vocab,'wb') as f:
    pickle.dump(vocabMap, f)


if os.path.isdir(args.WORD2VEC_MODEL):
  print('Loading TF word2vec model...')
  w2v_model = args.WORD2VEC_MODEL + '/model.ckpt-' + args.word2vec_epoch
  if args.word2vec_epoch == 'latest':
    w2v_model = tf.train.latest_checkpoint(args.WORD2VEC_MODEL)
    if w2v_model is None:
      print('No checkpoint candidates found. Exiting...')
      sys.exit(1)
    else:
      print('Loading latest checkpoint: ' +  w2v_model)
  elif args.word2vec_epoch != 'best' and not os.path.exists(w2v_model + '.meta'):
    print(w2v_model + ' not found! Exiting...')
    sys.exit(1)
  elif args.word2vec_epoch == 'best' and not os.path.exists(args.WORD2VEC_MODEL + '/best_model'):
    print(args.WORD2VEC_MODEL + '/best_model not found! Exiting...')
    sys.exit(1)
  else:
    with open(args.WORD2VEC_MODEL + '/best_model', 'r') as f:
      best_model = f.readlines()[0].strip() 
    w2v_model = args.WORD2VEC_MODEL + '/model.ckpt-' + best_model
    if not os.path.exists(w2v_model + '.meta'):
      print(w2v_model + ' not found! Exiting...')
      sys.exit(1)

  if not os.path.exists(w2v_model + '.meta'):
    print(w2v_model + ' not found! Exiting...')
    sys.exit(1)
  if args.vocab_path is None:
    vocabMap = OrderedDict(sorted(conf2vec_vocab.items(), key=lambda t:t[1]))
  w2v_embeddings = get_tf_embeddings(w2v_model, None if args.vocab_path is None else vocabMap.values(), conf2vec_flag=False)


print('Loading TF conf2vec model...')
c2v_embeddings = get_tf_embeddings(c2v_model, None if os.path.isdir(args.WORD2VEC_MODEL) and args.vocab_path is None else vocabMap.values(), conf2vec_flag=True, branched=args.branched_mode)


if args.concatenate:
  print('Concatenating models')
  c2v_embeddings = np.concatenate((w2v_embeddings, c2v_embeddings), axis=1)
  c2v_embeddings = ( c2v_embeddings / np.sqrt((c2v_embeddings ** 2).sum(-1))[..., np.newaxis]).astype(np.float32)
  if args.dump_concatenated_model:
    outfile1 = os.path.basename(os.path.dirname(args.WORD2VEC_MODEL)) if os.path.isfile(args.WORD2VEC_MODEL) else os.path.basename(os.path.abspath(args.WORD2VEC_MODEL))
    outfile2 = os.path.basename(os.path.abspath(args.MODELDIR))
    outfile = args.MODELDIR + '/' + outfile1 + '_' + outfile2 + '_concatenated.model'
    print('Dumping concatenated model to:', outfile)
    with open(outfile, 'w') as f:
      f.write(str(c2v_embeddings.shape[0])+ ' ' + str(c2v_embeddings.shape[1]) + os.linesep)
      np.savetxt(f, np.column_stack((vocabMap.keys(), c2v_embeddings)), delimiter=" ", fmt="%s")
      

print('Scoring analogies...')
t0 = time.time()
if args.analogies_original_word2vec:
  word2idx = [(w, idx) for idx, w in enumerate(w2v_model.index2word[:args.analogies_restrict_vocab])]
  w2v_embeddings = w2v_model.syn0norm[:args.analogies_restrict_vocab,:]
else:
  word2idx = [(w, idx) for idx, w in enumerate(vocabMap.keys()[:args.analogies_restrict_vocab])]
idx2word = {v:w for w, v in word2idx} if args.analogies_case_sensitive else {v:w.lower() for w, v in word2idx}
word2idx = {w.lower(): v for w, v in reversed(word2idx)} if not args.analogies_case_sensitive else dict(word2idx)

plot_tsne(w2v_embeddings, vocabMap, os.path.basename(os.path.dirname(args.WORD2VEC_MODEL)) if os.path.isfile(args.WORD2VEC_MODEL) else os.path.basename(os.path.abspath(args.WORD2VEC_MODEL)))
plot_tsne(c2v_embeddings, vocabMap, os.path.basename(os.path.abspath(args.MODELDIR)))

if args.apply_pca:
  print('W2V Embedding Dim:', w2v_embeddings.shape, 'C2V Embedding Dim:', c2v_embeddings.shape)
  print('Applying PCA transformation to the embedding space.')
  pca = PCA(n_components=2, whiten=args.pca_whiten)
  pca.fit(w2v_embeddings)
  w2v_embeddings =  pca.transform(w2v_embeddings)
  pca = PCA(n_components=2, whiten=args.pca_whiten)
  pca.fit(c2v_embeddings)
  c2v_embeddings =  pca.transform(c2v_embeddings)
  print('W2V Embedding Dim:', w2v_embeddings.shape, 'C2V Embedding Dim:', c2v_embeddings.shape)

if args.analogies:
  print('----------------word2vec - Nearest mode--------------------')
  read_and_score_analogies(word2idx, idx2word, w2v_embeddings[:args.analogies_restrict_vocab,:], case_insensitive=not args.analogies_case_sensitive)
  print('----------------conf2vec - Nearest mode--------------------')
  read_and_score_analogies(word2idx, idx2word, c2v_embeddings[:args.analogies_restrict_vocab,:], case_insensitive=not args.analogies_case_sensitive)
  print('----------------word2vec - top-2 mode--------------------')
  read_and_score_analogies(word2idx, idx2word, w2v_embeddings[:args.analogies_restrict_vocab,:], case_insensitive=not args.analogies_case_sensitive, mode='topk', k=2)
  print('----------------conf2vec - top-2 mode--------------------')
  read_and_score_analogies(word2idx, idx2word, c2v_embeddings[:args.analogies_restrict_vocab,:], case_insensitive=not args.analogies_case_sensitive, mode='topk', k=2)
  print('----------------word2vec - edit_dist=0.25 mode--------------------')
  read_and_score_analogies(word2idx, idx2word, w2v_embeddings[:args.analogies_restrict_vocab,:], case_insensitive=not args.analogies_case_sensitive, mode='edit_dist', thres=0.25)
  print('----------------conf2vec - edit_dist=0.25 mode--------------------')
  read_and_score_analogies(word2idx, idx2word, c2v_embeddings[:args.analogies_restrict_vocab,:], case_insensitive=not args.analogies_case_sensitive, mode='edit_dist', thres=0.25)
if args.homophone_analogies:
  print('-----------------word2vec - homophone-analogies----------------------')
  read_and_score_analogies(word2idx, idx2word, w2v_embeddings[:args.analogies_restrict_vocab,:], filename=args.homophone_analogies, case_insensitive=not args.analogies_case_sensitive, mode='nearest', dump=False)
  print('-----------------conf2vec - homophone-analogies----------------------')
  read_and_score_analogies(word2idx, idx2word, c2v_embeddings[:args.analogies_restrict_vocab,:], filename=args.homophone_analogies, case_insensitive=not args.analogies_case_sensitive, mode='nearest', dump=True)
  print('-----------------word2vec - homophone-analogies - top-2 mode----------------------')
  read_and_score_analogies(word2idx, idx2word, w2v_embeddings[:args.analogies_restrict_vocab,:], filename=args.homophone_analogies, case_insensitive=not args.analogies_case_sensitive, mode='topk', k=2, dump=False)
  print('-----------------conf2vec - homophone-analogies - top-2 mode----------------------')
  read_and_score_analogies(word2idx, idx2word, c2v_embeddings[:args.analogies_restrict_vocab,:], filename=args.homophone_analogies, case_insensitive=not args.analogies_case_sensitive, mode='topk', k=2)
  print('Finished in', time.time()-t0,'secs')


if args.human_ratings is not None:
  print('Computing Correlations...')
  t0 = time.time()
  if args.human_ratings_original_word2vec:
    word2idx = [(w, idx) for idx, w in enumerate(w2v_model.index2word)]
    w2v_embeddings = w2v_model.syn0norm
  else:
    word2idx = [(w, idx) for idx, w in enumerate(vocabMap.keys())]
  idx2word = {v:w for w, v in word2idx} if args.human_ratings_case_sensitive else {v:w.lower() for w, v in word2idx}
  word2idx = {w.lower(): v for w, v in reversed(word2idx)} if not args.human_ratings_case_sensitive else dict(word2idx)
  print('-----------------word2vec - human ratings correlations----------------------')
  score_human_correlation(word2idx, w2v_embeddings, case_insensitive=not args.human_ratings_case_sensitive)
  print('-----------------conf2vec - human ratings correlations----------------------')
  score_human_correlation(word2idx, c2v_embeddings, case_insensitive=not args.human_ratings_case_sensitive)
  if args.homophone_ratings:
    print('-----------------word2vec - homophone ratings correlations----------------------')
    score_human_correlation(word2idx, w2v_embeddings, filename=args.homophone_ratings, case_insensitive=not args.human_ratings_case_sensitive)
    print('-----------------conf2vec - homophone ratings correlations----------------------')
    score_human_correlation(word2idx, c2v_embeddings, filename=args.homophone_ratings, case_insensitive=not args.human_ratings_case_sensitive)
  print('Finished in', time.time()-t0,'secs')

